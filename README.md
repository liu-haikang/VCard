# vCard

## Introduction
> vCard, a file format standard for electronic business cards, is often attached to e-mail messages, and also can be exchanged in other ways, such as on the World Wide Web.
> It contains information such as names, addresses, phone numbers, URLs, logos, and photos. This library supports vCard standard 2.0 and 3.0.
> ![img1.png](img1_EN.png)

## How to Install
```shell
ohpm install @ohos/vcard
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### Generating a vCard
```
import {contact,VCardBuilder,VCardConfig} from "@ohos/vcard";

  let myCard= new  contact.Contact();
  let nickName = new contact.NickName();
  nickName.nickName ="Nickname";
  myCard.nickName = nickName;
  let builder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8");
  builder.appendNickNames(myCard);
  let result = builder.toString();

```

### Parsing a vCard Text
```
import fileio from "@ohos.fileio";
import {VCardParserImpI_V20,VCardEntry,VCardInterpreter,VCardProperty} from "@ohos/vcard";


let vCardEntry = new VCardEntry()
class MyVCardInterpreter implements VCardInterpreter{
    onVCardStarted(): void {
    }
    onVCardEnded(): void {
    }
    onEntryStarted(): void {
    }
    onEntryEnded(): void {
    }
    onPropertyCreated(property: VCardProperty): void {
        vCardEntry.addProperty(property)
    }
}
let myParser = new VCardParserImpl_V20()
myParser.addInterpreter(new MyVCardInterpreter() )
let fileDescriptor = fileio.openSync("./pathTo/00002.vcf", 0o100|0o2,0o666)
myParser.parse(fileDescriptor)
vCardEntry.consolidateFields()
let displayName = vCardEntry.getDisplayName()
let emails= vCardEntry.getEmailList()
```
## Available APIs
`let builder=new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8");`
1. Appends nicknames.
   `builder.appendNickNames()`
2. Appends names.
   `builder.appendNameProperties()`
3. Appends phone numbers.
   `builder.appendPhones()`
4. Appends emails.
   `builder.appendEmails()`
5. Appends addresses.
   `builder.appendPostals()`
6. Appends social networking tools.
   `builder.appendIms()`
7. Appends website addresses.
   `builder.appendWebsites()`
8. Appends organizations.
   `builder.appendOrganizations()`
9. Appends photo addresses.
   `builder.appendPhotos()`
10. Appends remarks.
   `builder.appendNotes()`
11. Appends groups.
    `builder.appendGroupMemberShip()`
12. Appends events.
    `builder.appendEvents()`
13. Appends relationships.
    `builder.appendRelation()`

## Constraints
This project has been verified in the following versions:

- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure
````
|---- VCard  
|     |---- entry  # Sample code
|     |---- library  # vCard library
|           |---- index.ets  # External APIs
|     |---- README.MD  # Readme                   
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-tpc/VCard/issues) or a [PR](https://gitee.com/openharmony-tpc/VCard/pulls).

## License
This project is licensed under the terms of the [Apache License 2.0](https://gitee.com/openharmony-tpc/VCard/blob/master/LICENSE).
